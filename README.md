# Protocol for CS4225 Chat Application #

PORT 4225 for the server



Table with:

Username : 16 Characters Alpha-Numeric

IP Address

Port

TimeStamp : Seconds since Epoch converted to local time.



Server deletes entries that are older than 100 seconds.





Requests

1) Add

ADD <username> ipaddress(xxx.yyy.zzz.uuu) port

OK

UNAVAILABLE



2) Update

UPDATE <username>

OK

NOTFOUND



3) Remove

REMOVE <username>

OK

NOTFOUND



4) Get

GET 