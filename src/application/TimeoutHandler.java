package application;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map;

/**
 * @author Dalton Schling and James Rucker
 *
 */
public class TimeoutHandler extends Thread {

	private int keepAlive;
	private Map<String, UserData> users;

	public TimeoutHandler(int keepAlive, Map<String, UserData> users) {
		this.keepAlive = keepAlive;
		this.users = users;
	}

	public void run() {
		while (true) {
			synchronized (this.users) {
				Iterator<String> it = this.users.keySet().iterator();
				while(it.hasNext()) {
					if (isTimedOut(it.next())) {
						it.remove();
					}
				}
			}
			try {
				Thread.sleep(2000 * (long) Math.random());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean isTimedOut(String username) {
		LocalDateTime timestamp = this.users.get(username).getTimestamp();
		LocalDateTime now = LocalDateTime.now();
		return timestamp.plusSeconds(this.keepAlive).isBefore(now);
	}
}
