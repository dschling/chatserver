/**
 * 
 */
package application;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;

/**
 * Chat server for CS4225
 * 
 * @author Dalton Schling and James Rucker
 *
 */
public class Server {

	public static void main(String[] arg) {
		final int KEEP_ALIVE = 10000;
		Map<String, UserData> users = new HashMap<>();
		final int PORT = 4225;
		boolean keepGoing = true;
		ServerSocket server = null;

		try {
			server = new ServerSocket(PORT);
		} catch (IOException err) {
			System.err.println("Error creating ServerSocket");
			System.exit(0);
		}
		;

		new TimeoutHandler(KEEP_ALIVE, users).start();
		try {
			while (keepGoing) {
				new ServerConnection(server.accept(), KEEP_ALIVE, users).start();
			}
			server.close();
		} catch (IOException err) {
			err.printStackTrace();
		}

	}
}
