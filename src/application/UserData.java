package application;

import java.time.LocalDateTime;

/**
 * @author Dalton Schling and James Rucker
 *
 */
public class UserData {

	private String ipAddress;
	private int port;
	private LocalDateTime timestamp;

	/**
	 * Constructor. Uses LocalDateTime.now() for timestamp
	 * 
	 * @param ip
	 * @param port
	 */
	public UserData(String ip, int port) {
		this.setIpAddress(ip);
		this.setPort(port);
		this.setTimestamp(LocalDateTime.now());
	}

	///
	/// Getters and Setters
	///

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress
	 *            the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @param port
	 *            the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @return the timestamp
	 */
	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

}
