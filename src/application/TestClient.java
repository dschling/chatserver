package application;

import java.net.*;
import java.io.*;
import java.util.Scanner;

/**
 * @author Dalton Schling and James Rucker
 *
 */
public class TestClient {

	public static void main(String[] arg) throws IOException {

		String addr = "localhost";
		int port = 4225;
		Scanner scan = new Scanner(System.in);

		Socket socket = new Socket(addr, port);
		BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

		while (!socket.isClosed()) {
			do {
				System.out.println(in.readLine());
			} while (socket.isConnected() && in.ready());
			System.out.flush();
			String line = scan.nextLine();
			if (line.equals("exit")) {
				socket.close();
				scan.close();
				System.exit(0);
			}
			out.println(line);
		}
	}
}
