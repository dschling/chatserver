package application;

import java.net.*;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.io.*;

/**
 * @author Dalton Schling and James Rucker
 *
 */
public class ServerConnection extends Thread {

	private Map<String, UserData> users;
	private Socket socket;
	private final int timeout;
	private BufferedReader in;
	private PrintWriter out;

	public ServerConnection(Socket socket, int timeout, Map<String, UserData> users) {
		this.socket = socket;
		this.timeout = timeout;
		this.users = users;
		try {
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.out = new PrintWriter(socket.getOutputStream(), true);
		} catch (IOException err) {
			err.printStackTrace();
		}
		this.sendWelcomeMessage();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		this.out.flush();
		boolean keepGoing = true;
		try {
			while (keepGoing) {
				String line = this.in.readLine();

				synchronized (this.users) {
					this.process(line);
				}

			}
			socket.close();
		} catch (IOException err) {
			err.printStackTrace();
		}
	}

	private void sendWelcomeMessage() {
		this.out.println("Welcome to CS4225 list! Keep alive time set to " + this.timeout);
	}

	private void add(String username, String ip, int port) {
		if (!Pattern.matches("[a-zA-Z1-9]{1,16}", username)) {
			this.out.println("Username must be alpha-numeric and no more than 16 characters.");
			return;
		}

		if (this.users.containsKey(username)) {
			this.out.println("UNAVAILABLE");
			return;
		}

		this.users.put(username, new UserData(ip, port));
		this.out.println("OK");
	}

	private void update(String username) {
		if (!this.users.containsKey(username)) {
			this.out.println("NOTFOUND");
			return;
		}

		this.users.get(username).setTimestamp(LocalDateTime.now());
		this.out.println("OK");
	}

	private void remove(String username) {
		if (!this.users.containsKey(username)) {
			this.out.println("NOTFOUND");
			return;
		}

		this.users.remove(username);
		this.out.println("OK");
	}

	private void get() {
		Set<String> usernames = this.users.keySet();
		this.out.println("Number of users: " + usernames.size());
		for (String username : usernames) {
			UserData user = this.users.get(username);
			this.out.println(username + " " + user.getIpAddress() + " " + user.getPort());
		}
	}

	private void process(String line) {
		processAdd(line);
		processUpdate(line);
		processRemove(line);
		processGet(line);
	}

	private void processGet(String line) {
		if (line.toLowerCase().equals("get")) {
			this.get();
		}
	}

	private void processRemove(String line) {
		if (line.toLowerCase().startsWith("remove")) {
			String[] parts = line.split(" ");
			String username = null;
			try {
				username = parts[1];
			} catch (IndexOutOfBoundsException ex) {
				this.out.println("NOTFOUND");
				return;
			}
			this.remove(username);
		}
	}

	private void processUpdate(String line) {
		if (line.toLowerCase().startsWith("update")) {
			String[] parts = line.split(" ");
			String username = null;
			try {
				username = parts[1];
			} catch (IndexOutOfBoundsException ex) {
				this.out.println("NOTFOUND");
				return;
			}
			this.update(username);
		}
	}

	private void processAdd(String line) {
		if (line.toLowerCase().startsWith("add")) {
			String[] parts = line.split(" ");
			String username = null;
			String ip = null;
			int port = 0;
			try {
				username = parts[1];
				ip = parts[2];
				port = Integer.parseInt(parts[3]);
			} catch (IndexOutOfBoundsException | NumberFormatException ex) {
				this.out.println("UNAVAILABLE");
				return;
			}
			this.add(username, ip, port);
		}
	}
}
